﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrWebHookHistory : AbRepository<TR_WEB_HOOK_HISTORY>
    {
        public override TR_WEB_HOOK_HISTORY GetData(string id)
        {
            return this.FindBy(a => a.MESSAGE_ID == id).FirstOrDefault();
        }

        public override List<TR_WEB_HOOK_HISTORY> GetDatas(string id)
        {
            return this.FindBy(a => a.ACTION_TYPE == id).ToList();
        }

        public GetWebHookHistoryData Save(GetWebHookHistoryData request)
        {
            this.Data = new TR_WEB_HOOK_HISTORY();
            this.Data.MESSAGE_ID = request.message_id;
            this.Data.ACTION_TYPE = request.action_type;
            this.Data.EMAIL = request.email;
            this.Data.SOURCE = request.source;
            this.Data.DEVICE = request.device;
            this.Data.SOURCE_TRANSECTION = request.source_transaction;
            this.Data.CAMPAINGN_ID = request.campaign_id.ToString();
            this.Data.CAMPAINGN_NAME = request.campaign_name;
            this.Data.DATE_TIME = request.datetime;
            this.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
            this.Data.CREATE_BY = "";
            this.Data.CREATE_DATE = DateTime.Now;

            this.Create(this.Data);

            return request;

        }
    }
    public class TrWebHookHistoryDto : GetWebHookHistoryData
    {
        public string id { get; set; }
        public string customerId { get; set; }
        public DateTime? appointmentDate { get; set; }
        public string addressSurvey { get; set; }
        public string venderName { get; set; }
        public string venderCode { get; set; }
        public string status { get; set; }
    }
}