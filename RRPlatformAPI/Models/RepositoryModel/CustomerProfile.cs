﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class CustomerProfile : AbRepository<CUSTOMER_PROFILE>
    {
        public override CUSTOMER_PROFILE GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.CUSTOMER_ID == idInt).FirstOrDefault();
        }

        public  CUSTOMER_PROFILE GetDataByCustomerNo(string customerNo)
        {
            return this.FindBy(a => a.CUSTOMER_NO == customerNo && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public List<CUSTOMER_PROFILE> FilterByGetCustomerList(GetCustomerListRequest request)
        {
            this.Datas = new List<CUSTOMER_PROFILE>();
            this.Datas = this.FindBy(a => a.IS_ACTIVE == "Y").ToList();

            if (!request.campianCode.IsNullOrEmptyWhiteSpace())
            {
                int campianCode = request.campianCode.ToInt32();
                this.Datas = this.Datas.Where(a => a.CUSTOMER_ID == campianCode).ToList();
            }
            if (!request.cano.IsNullOrEmptyWhiteSpace())
            {
                this.Datas = this.Datas.Where(a => a.CA_NO == request.cano).ToList();
            }
            if (!request.customerNo.IsNullOrEmptyWhiteSpace())
            {
                 this.Datas = this.Datas.Where(a => a.CUSTOMER_NO == request.customerNo).ToList();
            }
            if (!request.customerStatus.IsNullOrEmptyWhiteSpace())
            {
                int activityStatus = request.customerStatus.ToInt32();
                this.Datas = this.Datas.Where(a => a.ACTIVITY_STATUS == activityStatus).ToList();
            }

            if (!request.tel1.IsNullOrEmptyWhiteSpace())
            {
                this.Datas = this.Datas.Where(a => a.TEL01 == request.tel1).ToList();
            }
            if (!request.tel2.IsNullOrEmptyWhiteSpace())
            {
                this.Datas = this.Datas.Where(a => a.TEL02 == request.tel2).ToList();
            }
            if (!request.tel3.IsNullOrEmptyWhiteSpace())
            {
                this.Datas = this.Datas.Where(a => a.TEL03 == request.tel3).ToList();
            }

            return this.Datas;
        }

        

        public CUSTOMER_PROFILE Save(CustomerProfileDto customerData)
        {
            CUSTOMER_PROFILE result = new CUSTOMER_PROFILE();
            this.Data = this.GetData(customerData.customerId);

            if (this.Data == null)
            {
                this.Data = this.BindDataDb(this.Data, customerData, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataDb(this.Data, customerData, ActionType.Add);
                this.Edit("",this.Data);
            }

            result = this.Data;

            return result;
        }

        public CUSTOMER_PROFILE BindDataDb(CUSTOMER_PROFILE data, CustomerProfileDto dataBind,ActionType actionType)
        {
            if (data != null)
                data = new CUSTOMER_PROFILE();

            data.CA_NO = dataBind.caNo;
            data.CUSTOMER_NO = dataBind.customerId;
            data.ADDRESS = dataBind.address;
            data.SUBDISTRICT = dataBind.subdistrict;
            data.DISTRICT = dataBind.district;
            data.PROVINCE = dataBind.province;
            data.REGION = dataBind.region;
            data.TEL01 = dataBind.tel1;
            data.TEL02 = dataBind.tel2;
            data.TEL03 = dataBind.tel3;
            data.AVERAGE_AMOUNT_OF_ELECTRICCITY_USAGE = dataBind.averageAmountOfElectricityUsage.ToString();
            data.SUMMARY_AMOUNT_OF_ELECTRICCITY_USAGE = dataBind.sumaryAmountOfElectricityUsage.ToString();
            data.SUB_BUSSINESS_TYPE = dataBind.subBusinessType.ToString();
            data.ACTIVITY_STATUS = dataBind.customerStatus.ToInt32();
            data.IS_ACTIVE = dataBind.isActive;

            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = dataBind.createBy;
                data.CREATE_DATE = dataBind.createDate;
            }
            else
            {
                data.UPDATE_BY = dataBind.updateBy;
                data.UPDATE_DATE = dataBind.updateDate;
            }
            return data;
        }

        public CustomerProfileDto BindDataDto(CUSTOMER_PROFILE dataBind)
        {
            CustomerProfileDto data = new CustomerProfileDto();
            SourceData sourceData = new SourceData();

            data.customerId = dataBind.CUSTOMER_ID.ToString();
            data.caNo = dataBind.CA_NO;
            data.customerNo = dataBind.CUSTOMER_NO;
            data.address = dataBind.ADDRESS;
            data.subdistrict = dataBind.SUBDISTRICT;
            data.district = dataBind.DISTRICT;
            data.province = dataBind.PROVINCE;
            data.region = dataBind.REGION;
            data.tel1 = dataBind.TEL01;
            data.tel2 = dataBind.TEL02;
            data.tel3 = dataBind.TEL03;
            data.averageAmountOfElectricityUsage = dataBind.AVERAGE_AMOUNT_OF_ELECTRICCITY_USAGE.ToString();
            data.sumaryAmountOfElectricityUsage = dataBind.SUMMARY_AMOUNT_OF_ELECTRICCITY_USAGE.ToString();
            data.subBusinessType = dataBind.SUB_BUSSINESS_TYPE.ToString();
            data.dataSource = sourceData.GetSourceNameByCustomerId(dataBind.CUSTOMER_ID);
            data.customerStatus = dataBind.ACTIVITY_STATUS.ToString();
            data.isActive = dataBind.IS_ACTIVE;
            data.createBy = dataBind.CREATE_BY;
            data.createDate = dataBind.CREATE_DATE;
            data.updateBy = dataBind.UPDATE_BY;
            data.updateDate = dataBind.UPDATE_DATE;
            return data;
        }

        public override List<CUSTOMER_PROFILE> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }

    public class CustomerProfileDto : MasterDto
    {
        public string customerId { get; set; }
        public string campianCode { get; set; }
        public string caNo { get; set; }
        public string customerNo { get; set; }
        public string address { get; set; }
        public string subdistrict { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string region { get; set; }
        public string tel1 { get; set; }
        public string tel2 { get; set; }
        public string tel3 { get; set; }
        public string averageAmountOfElectricityUsage { get; set; }
        public string sumaryAmountOfElectricityUsage { get; set; }
        public string subBusinessType { get; set; }
        public List<string> dataSource { get; set; }
        public string customerStatus { get; set; }
    }
}