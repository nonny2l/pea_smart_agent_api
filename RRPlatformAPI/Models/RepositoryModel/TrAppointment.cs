﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrAppointment : AbRepository<TR_APPOINTMENT>
    {
        public override TR_APPOINTMENT GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.ID == idInt).FirstOrDefault();
        }
        public TR_APPOINTMENT GetDataByCustomerIdAnsVender(int customerId,string venderCode)
        {
            return this.FindBy(a => a.CUSTOMER_ID == customerId && a.VENDER_CODE == venderCode && a.IS_ACTIVE == "Y").FirstOrDefault();
        }
        public List<TR_APPOINTMENT> GetDataByCustomerId(int customerId)
        {
            return this.FindBy(a => a.CUSTOMER_ID == customerId && a.IS_ACTIVE == "Y").ToList();
        }


        public TrAppointmentDto BindDataDto(TR_APPOINTMENT dataBind)
        {
            TrAppointmentDto data = new TrAppointmentDto();

            data.id = dataBind.ID.ToString();
            data.customerId = dataBind.CUSTOMER_ID.ToString();
            data.addressSurvey = dataBind.ADDRESS_SURVEY;
            data.appointmentDate = dataBind.APPOINTMENT_DATE;
            data.venderCode = dataBind.VENDER_CODE;
            data.venderName = dataBind.VENDER_NAME;
            data.status = dataBind.STATUS.ToString();
            data.isActive = dataBind.IS_ACTIVE;
            data.createBy = dataBind.CREATE_BY;
            data.createDate = dataBind.CREATE_DATE;
            data.updateBy = dataBind.UPDATE_BY;
            data.updateDate = dataBind.UPDATE_DATE;

           
            return data;
        }

        public List<TR_APPOINTMENT> GetListByCustomerId(int customerId)
        {
            return this.FindBy(a => a.CUSTOMER_ID == customerId).ToList();
        }


        public TR_APPOINTMENT BindDataDb(TR_APPOINTMENT data, TrAppointmentDto dataBind, ActionType actionType)
        {
            if (data == null)
                data = new TR_APPOINTMENT();

            data = this.BindObjectToDB(dataBind);
          
            //if (actionType == ActionType.Add)
            //{
            //    data.CREATED_BY = dataBind.createBy;
            //    data.CREATED_DATE = dataBind.createDate;
            //}
            //else
            //{
            //    data.UPDATED_BY = dataBind.updateBy;
            //    data.UPDATED_DATE = dataBind.updateDate;
            //}
            return data;
        }

        public TR_APPOINTMENT Save(TrAppointmentDto request)
        {
            this.Data = GetDataByCustomerIdAnsVender(request.customerId.ToInt32(), request.venderCode);

            if (this.Data == null)
            {
                this.Data = this.BindDataDb(this.Data, request, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataDb(this.Data, request, ActionType.Edit);
                this.Edit("",this.Data);
            }

            return this.Data;
        }
        public override List<TR_APPOINTMENT> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
    public class TrAppointmentDto : MasterDto
    {
        public string id { get; set; }
        public string customerId { get; set; }
        public DateTime? appointmentDate { get; set; }
        public string addressSurvey { get; set; }
        public string venderName { get; set; }
        public string venderCode { get; set; }
        public string status { get; set; }
    }
}