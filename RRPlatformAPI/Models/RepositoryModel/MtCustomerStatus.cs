﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtCustomerStatus : AbRepository<MT_CUSTOMER_STATUS>
    {
        public override MT_CUSTOMER_STATUS GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.ID == idInt).FirstOrDefault();
        }
        public MtCustomerStatusDto BindData(MT_CUSTOMER_STATUS dataBind)
        {
            MtCustomerStatusDto data = new MtCustomerStatusDto();

            data.statusId = dataBind.ID.ToString();
            data.statusName = dataBind.STATUS_NAME;
            data.isActive = dataBind.IS_ACTIVE;
         
            return data;
        }

        public override List<MT_CUSTOMER_STATUS> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
    public class MtCustomerStatusDto : MasterDto
    {
        public string statusId { get; set; }
        public string statusName { get; set; }
    }

    public class MasterDto
    {
        public string isActive { get; set; }
        public string createBy { get; set; }
        public DateTime? createDate { get; set; }
        public string updateBy { get; set; }
        public DateTime? updateDate { get; set; }
    }
}