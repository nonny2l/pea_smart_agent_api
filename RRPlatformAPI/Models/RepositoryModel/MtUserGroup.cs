﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtUserGroup : AbRepository<MT_USER_GROUP>
    {
        public MtUserGroup()
        {
            this.Data = new MT_USER_GROUP();
            this.Datas = new List<MT_USER_GROUP>();
        }

        public override MT_USER_GROUP GetData(string id)
        {
            int idInt = Convert.ToInt32(id);
            var data = (from d in DbContext.MT_USER_GROUP
                        where d.USER_GROUP_ID == idInt
                                 select d).FirstOrDefault();

            return data;
        }
        public MT_USER_GROUP BindDataSave(string name, string desc, string active, string userName, MT_USER_GROUP data, ActionType action)
        {
            if (data == null)
                data = new MT_USER_GROUP();

            data.NAME = name;
            data.DESCRIPTION = desc;
            data.IS_ACTIVE = active;

            if (action == ActionType.Add)
            {
                data.CREATE_BY = userName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userName;
                data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

        public override List<MT_USER_GROUP> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public bool SaveById(int id, string name, string desc, string active, string userName)
        {
            bool result = false;
            this.Data = this.GetData(id.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindDataSave(name, desc, active, userName, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSave(name, desc, active, userName, this.Data, ActionType.Edit);
                this.Edit("",this.Data);
            }


            return result;
        }

    }
}