﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtConficH : AbRepository<MT_CONFIG_H>
    {
        public override MT_CONFIG_H GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.CONFIG_H_ID == idInt).FirstOrDefault();
        }
        public  MT_CONFIG_H GetDataByCode(string configHCode)
        {
            return this.FindBy(a => a.CONFIG_H_CODE == configHCode && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public MtConficHDto BindDataDto(MT_CONFIG_H dataBind)
        {
            MtConficHDto data = new MtConficHDto();
            data.mtConfigHId = dataBind.CONFIG_H_ID.ToString();
            data.mtConfigHCode = dataBind.CONFIG_H_CODE.ToString();
            data.mtConfigHTName = dataBind.CONFIG_H_TNAME;
            data.mtConfigHEName = dataBind.CONFIG_H_ENAME;
            data.isActive = dataBind.IS_ACTIVE;
            data.createBy = dataBind.CREATE_BY;
            data.createDate = dataBind.CREATE_DATE;
            data.updateBy = dataBind.UPDATE_BY;
            data.updateDate = dataBind.UPDATE_DATE;

            return data;
        }

      
        public override List<MT_CONFIG_H> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
    public class MtConficHDto : MasterDto
    {
        public string mtConfigHId { get; set; }
        public string mtConfigHCode { get; set; }
        public string mtConfigHTName { get; set; }
        public string mtConfigHEName { get; set; }
       
    }
}