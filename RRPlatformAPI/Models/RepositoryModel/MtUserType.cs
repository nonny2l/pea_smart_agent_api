﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtUserType : AbRepository<MT_USER_TYPE>
    {

        public MtUserType()
        {
            this.Data = new MT_USER_TYPE();
            this.Datas = new List<MT_USER_TYPE>();
        }

        public override MT_USER_TYPE GetData(string id)
        {
            int idInt = Convert.ToInt32(id);
            var data = (from d in DbContext.MT_USER_TYPE
                        where d.USER_TYPE_ID == idInt
                        select d).FirstOrDefault();

            return data;
        }

      
        public override List<MT_USER_TYPE> GetDatas(string id)
        {
            int idInt = Convert.ToInt32(id);
            var data = (from d in DbContext.MT_USER_TYPE
                        where d.USER_TYPE_ID == idInt
                        select d).ToList();

            return data;
        }

        public MT_USER_TYPE BindDataSave(SaveMtUserTypeRequest dataBind, MT_USER_TYPE data, ActionType action)
        {
            if (data == null)
                data = new MT_USER_TYPE();

            data.USER_TYPE_NAME = dataBind.userTypeName;
            data.IS_ACTIVE = dataBind.isActive;
           
            if (action == ActionType.Add)
            {
                data.CREATE_BY = "";
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = "";
                data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

        public MT_USER_TYPE CheckaSave(SaveMtUserTypeRequest data)
        {
            this.Data = this.GetData(data.userTypeId);

            if (this.Data == null)
            {
                this.Data = this.BindDataSave(data, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSave(data, this.Data, ActionType.Edit);
                this.Edit("", this.Data);
            }

            return this.Data;
        }

    }

    public class MtUserTypeData
    {
        public string userTypeId { get; set; }
        public string userTypeName { get; set; }
        public string isActive { get; set; }
        public string createBy { get; set; }
        public DateTime? createDate { get; set; }
        public string updateBy { get; set; }
        public DateTime? updateDate { get; set; }
    }
}