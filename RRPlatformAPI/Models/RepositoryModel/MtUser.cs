﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtUser : AbRepository<MT_USER>
    {
        public override MT_USER GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.USER_ID == idInt).FirstOrDefault();
        }

        public ActionResultStatus CheckLogIn(string userName,string password,int userGroup)
        {
            ActionResultStatus result = new ActionResultStatus();

            this.Data = new MT_USER();
            this.Data = this.FindBy(a => a.USERNAME == userName).FirstOrDefault();
            if (this.Data != null)
            {
                if (password != this.Data.PASSWORD)
                {
                    result.success = false;
                    result.code = (int)StatusCode.PasswordInCorrect;
                    result.message = StatusCode.PasswordInCorrect.Value();
                    result.description = "รหัสผ่านของคุณไม่ถูกต้อง";
                    return result;
                }
                else if (userGroup != this.Data.USER_GROUP_ID)
                {
                    result.success = false;
                    result.code = (int)StatusCode.NoPermission;
                    result.message = StatusCode.NoPermission.Value();
                    result.description = "กลุ่มผู้ใช้งานของคุณไม่ถูกต้อง";
                }
                else
                {
                    UserAccess userData = BindData(this.Data);
                    result.data = userData;
                    result.key = JwtManagement.GenerateJasonWebToken(userData);
                }
            }
            else
            {
                result.success = false;
                result.code = (int)StatusCode.NoPermission;
                result.message = StatusCode.NoPermission.Value();
                result.description = "ไม่มีผู้ใช้งานนี้ในระบบ";
            }


            return result;
        }

        public ActionResultStatus CheckLogOut(string userId)
        {
            ActionResultStatus result = new ActionResultStatus();

            this.Data = new MT_USER();
            this.Data = this.GetData(userId);
            if (this.Data != null)
            {
                result.success = false;
                result.code = (int)StatusCode.NoPermission;
                result.message = StatusCode.NoPermission.Value();
                result.description = "ไม่มีผู้ใช้งานนี้ในระบบ";
            }
          
            return result;
        }

        public UserAccess BindData(MT_USER dataBind)
        {
            UserAccess data = new UserAccess();

            data.userId = dataBind.USER_ID.ToString();
            data.email = dataBind.EMAIL;
            data.userName = dataBind.USERNAME;
            data.userGroupId = dataBind.USER_GROUP_ID.ToString();
            data.userTypeId = dataBind.USER_TYPE_ID.ToString();
            data.isActive = dataBind.IS_ACTIVE;
            data.empNo = dataBind.EMP_NO;
            return data;
        }

        public override List<MT_USER> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
}