﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class SourceData : AbRepository<SOURCE_DATA>
    {
        public override SOURCE_DATA GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.ID == idInt).FirstOrDefault();
        }

        public SOURCE_DATA CheckDupicate(int customerId,string sourceName)
        {
            return this.FindBy(a => a.CUSTOMER_ID == customerId && a.SOURCE_NAME == sourceName && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public List<string> GetSourceNameByCustomerId(int customerId)
        {
            return this.FindBy(a => a.CUSTOMER_ID == customerId && a.IS_ACTIVE == "Y").Select(a => a.SOURCE_NAME).ToList();
        }

        public SourceDataDto BindDataDto(SOURCE_DATA dataBind)
        {
            SourceDataDto data = new SourceDataDto();

            data.id = dataBind.ID.ToString();
            data.customerId = dataBind.CUSTOMER_ID.ToString();
            data.sourceName = dataBind.SOURCE_NAME;
            data.isActive = dataBind.IS_ACTIVE;
            data.createBy = dataBind.CREATE_BY;
            data.createDate = dataBind.CREATE_DATE;
            data.updateBy = dataBind.UPDATE_BY;
            data.updateDate = dataBind.UPDATE_DATE;
            return data;
        }

        public SOURCE_DATA BindDataDb(SOURCE_DATA data ,int customerId, string sourceName,ActionType actionType)
        {
            if (data == null)
                data = new SOURCE_DATA();

            data.CUSTOMER_ID = customerId;
            data.SOURCE_NAME = sourceName;
            data.IS_ACTIVE = StatusYesNo.Yes.Value();

            //if (actionType == ActionType.Add)
            //{
            //    data.CREATED_BY = dataBind.createBy;
            //    data.CREATED_DATE = dataBind.createDate;
            //}
            //else
            //{
            //    data.UPDATED_BY = dataBind.updateBy;
            //    data.UPDATED_DATE = dataBind.updateDate;
            //}
            return data;
        }

        public override List<SOURCE_DATA> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public SOURCE_DATA Save(int customerId,string sourceName)
        {
            SOURCE_DATA result = new SOURCE_DATA();
            this.Data = this.CheckDupicate(customerId,sourceName);

            if (this.Data == null)
            {
                this.Data = this.BindDataDb(this.Data, customerId, sourceName, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataDb(this.Data, customerId, sourceName, ActionType.Add);
                this.Edit("", this.Data);
            }

            result = this.Data;

            return result;
        }
    }
    public class SourceDataDto : MasterDto
    {
        public string id { get; set; }
        public string customerId { get; set; }
        public string sourceName { get; set; }
    }
}