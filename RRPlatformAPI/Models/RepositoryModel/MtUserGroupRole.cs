﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtUserGroupRole : AbRepository<MT_USER_GROUP_ROLE>
    {
        public MtUserGroupRole()
        {
            this.Data = new MT_USER_GROUP_ROLE();
            this.Datas = new List<MT_USER_GROUP_ROLE>();
        }

        public override MT_USER_GROUP_ROLE GetData(string id)
        {
            int idInt = Convert.ToInt32(id);
            var data = (from d in DbContext.MT_USER_GROUP_ROLE
                        where d.USER_ID == idInt
                                 select d).FirstOrDefault();

            return data;
        }
       
        public override List<MT_USER_GROUP_ROLE> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
   
    }
}