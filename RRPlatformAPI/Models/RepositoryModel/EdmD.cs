﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class EdmD : AbRepository<EDM_D>
    {
        public override EDM_D GetData(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.ID == idInt && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public override List<EDM_D> GetDatas(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.EDM_H_ID == idInt && a.IS_ACTIVE == "Y").ToList();
        }

        public EDM_D CheckDupicate(int edmHId, string email,string actionType)
        {
            return FindBy(a => a.EDM_H_ID == edmHId
                              && a.EMAIL == email
                              && a.ACTION_TYPE == actionType
                              && a.IS_ACTIVE == "Y").FirstOrDefault();
        }
    }
}