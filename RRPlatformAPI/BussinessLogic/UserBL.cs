﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;

namespace RRPlatFormAPI.BussinessLogic
{
    public class UserBL : AbBLRepository
    {

        public ActionResultStatus Login(LoginRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            MtUser mtUser = new MtUser();
            try
            {
                mReturn = mtUser.CheckLogIn(request.userName, request.passWord, request.userGroupId);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus LogOut(UserAccess request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            MtUser mtUser = new MtUser();
            try
            {
                mReturn = mtUser.CheckLogOut(request.userId);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }


        public ActionResultStatus GetUserList(GetUserListRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                List<GetMtUserListResponse> response = new List<GetMtUserListResponse>();
              
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
    }
}