﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace RRPlatFormAPI.BussinessLogic
{
    public class MasterBL : AbBLRepository
    {

        public ActionResultStatus GetMasterList(GetMasterListRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            MtConficH mtConficH = new MtConficH();
            MtConficD mtConficD = new MtConficD();
            try
            {
                List<GetMasterListResponse> responseRecursive = new List<GetMasterListResponse>();
                List<GetMasterListResponse> responseResponse = new List<GetMasterListResponse>();
                mtConficH.Data = mtConficH.GetDataByCode(request.configHCode);
                if (mtConficH.Data != null)
                {
                    mtConficD.Datas = mtConficD.GetDatas(mtConficH.Data.CONFIG_H_ID.ToString());
                    foreach (var mtConficDData in mtConficD.Datas)
                    {
                        MtConficDDto dataDto = mtConficD.BindDataDto(mtConficDData);
                        string dataDtoString = JsonConvert.SerializeObject(dataDto);
                        GetMasterListResponse responseData = JsonConvert.DeserializeObject<GetMasterListResponse>(dataDtoString);
                        responseRecursive.Add(responseData);
                    }

                    responseResponse = responseRecursive.Where(c => c.parentDId == "0" && c.isActive == "Y")
                    .Select(dataBind => new GetMasterListResponse
                    {
                        mtConfigDId = dataBind.mtConfigDId,
                        mtConfigHId = dataBind.mtConfigHId,
                        mtConfigDSeq = dataBind.mtConfigDSeq,
                        mtConfigDTName = dataBind.mtConfigDTName,
                        mtConfigDEName = dataBind.mtConfigDEName,
                        parentDId = dataBind.parentDId,
                        isActive = dataBind.isActive,
                        createBy = dataBind.createBy,
                        createDate = dataBind.createDate,
                        updateBy = dataBind.updateBy,
                        updateDate = dataBind.updateDate,
                        child = mtConficD.GetChildParent(responseRecursive, dataBind.mtConfigDId)

                    }).ToList();

                    mReturn.data = responseResponse;

                }
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

    }
}