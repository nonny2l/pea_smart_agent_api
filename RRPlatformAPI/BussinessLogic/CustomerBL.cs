﻿using Newtonsoft.Json;
using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace RRPlatFormAPI.BussinessLogic
{
    public class CustomerBL : AbBLRepository
    {

        public ActionResultStatus GetMasterCustomerStatus()
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            MtCustomerStatus mtCustomerStatus = new MtCustomerStatus();
            List<MtCustomerStatusDto> resultList = new List<MtCustomerStatusDto>();
            try
            {
                mtCustomerStatus.Datas = mtCustomerStatus.GetAll().ToList();
                foreach (var data in mtCustomerStatus.Datas)
                {
                    resultList.Add(mtCustomerStatus.BindData(data));
                }

                mReturn.data = resultList;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus SaveImportCustomer(SaveImportCustomerRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            CustomerProfile customerProfile = new CustomerProfile();
            SourceData sourceData = new SourceData();
            try
            {
                foreach (var customerData in request.customerList)
                {
                    customerProfile.Data = customerProfile.Save(customerData);
                    foreach (var dataSourceName in customerData.dataSource)
                    {
                        sourceData.Save(customerProfile.Data.CUSTOMER_ID, dataSourceName);
                    }
                }
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus GetCustomerList(GetCustomerListRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            CustomerProfile customerProfile = new CustomerProfile();
            TrForrow trForrow = new TrForrow();
            TrAppointment trAppointment = new TrAppointment();
            List<GetCustomerListResponse> resultList = new List<GetCustomerListResponse>();
            try
            {
                
            


                GetCustomerListResponse listResponse = new GetCustomerListResponse();
                customerProfile.Datas = customerProfile.FilterByGetCustomerList(request);
                if (customerProfile.Datas.Count != 0)
                {
                    foreach (var customerProfileData in customerProfile.Datas)
                    {
                        CustomerProfileDto customerProfileDto = customerProfile.BindDataDto(customerProfileData);
                        string customerProfileDtoString = JsonConvert.SerializeObject(customerProfileDto);
                        listResponse = JsonConvert.DeserializeObject<GetCustomerListResponse>(customerProfileDtoString);
                        listResponse.followList = new List<TrForrowDto>();

                        trForrow.Datas = new List<TR_FOLLOW>();
                        trForrow.Datas = trForrow.GetListByCustomerId(customerProfileData.CUSTOMER_ID);
                        if (trForrow.Datas.Count != 0)
                        {
                            foreach (var trForrowData in trForrow.Datas)
                            {
                                listResponse.followList.Add(trForrow.BindDataDto(trForrowData));
                            }
                        }


                        trAppointment.Datas = new List<TR_APPOINTMENT>();
                        trAppointment.Datas = trAppointment.GetListByCustomerId(customerProfileData.CUSTOMER_ID);
                        if (trAppointment.Datas.Count != 0)
                        {
                            foreach (var trAppointmentData in trAppointment.Datas)
                            {
                                listResponse.appointmentList.Add(trAppointment.BindDataDto(trAppointmentData));
                            }
                        }
                    }
                }

       
                mReturn.data = listResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus UpdateCustomerStatus(UpdateCustomerStatusRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            CustomerProfile customerProfile = new CustomerProfile();
            try
            {
                foreach (var customerData in request.customerList)
                {
                    customerProfile.Data = customerProfile.GetDataByCustomerNo(customerData.customerNo);
                    customerProfile.Data.ACTIVITY_STATUS = customerData.customerStatus.ToInt32();
                    customerProfile.Edit("",customerProfile.Data);
                }
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus SaveAppointment(SaveAppointmentRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            TrAppointment trAppointment = new TrAppointment();
            try
            {
                trAppointment.Save(request);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus GetAppointmentList(GetAppointmentListRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            TrAppointment trAppointment = new TrAppointment();
            List<TrAppointmentDto> resultList = new List<TrAppointmentDto>();
            try
            {
                trAppointment.Datas = trAppointment.GetDataByCustomerId(request.customerId.ToInt32());
                foreach (var trAppointmentData in trAppointment.Datas)
                {
                    resultList.Add(trAppointment.BindDataDto(trAppointmentData));
                }

                mReturn.data = resultList;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
    }
}