﻿using Newtonsoft.Json;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.Controllers.ExternalApiCaller;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Specialized;

namespace RRPlatFormAPI.BussinessLogic
{
    public class TaxiBL : AbBLRepository
    {

        public ActionResultStatus GetWebHookHistory(NameValueCollection request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            TrWebHookHistory trWebHookHistory = new TrWebHookHistory();
            EdmH edmH = new EdmH();
            EdmD edmD = new EdmD();
            try
            {
                string reportData = request["report"];
                var requestBody = JsonConvert.DeserializeObject<GetWebHookHistoryData>(reportData);

              
                string[] messageList = requestBody.message_id.Split('|');
                string messageId = messageList[0];


                edmH.Data = edmH.GetByMessageIdEmail(messageId, requestBody.email);
                if (edmH.Data != null)
                {
                    edmD.Data = edmD.CheckDupicate(edmH.Data.ID, requestBody.email, requestBody.action_type);
                    if (edmD.Data == null)
                    {
                        edmD.Data = new EDM_D();
                        edmD.Data.EDM_H_ID = edmH.Data.ID;
                        edmD.Data.ACTION_TYPE = requestBody.action_type;
                        edmD.Data.EMAIL = requestBody.email;
                        edmD.Data.SOURCE = requestBody.source;
                        edmD.Data.DEVICE = requestBody.device;
                        edmD.Data.SOURCE_TRANSECTION = requestBody.source_transaction;
                        edmD.Data.CAMPAINGN_ID = requestBody.campaign_id.ToString();
                        edmD.Data.CAMPAINGN_ID = requestBody.campaign_name;
                        edmD.Data.CREATE_DATE = DateTime.Now;
                        edmD.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                        edmD.Create(edmD.Data);
                    }
                    else
                    {
                        edmD.Data.ACTION_TYPE = requestBody.action_type;
                        edmD.Data.EMAIL = requestBody.email;
                        edmD.Data.SOURCE = requestBody.source;
                        edmD.Data.DEVICE = requestBody.device;
                        edmD.Data.SOURCE_TRANSECTION = requestBody.source_transaction;
                        edmD.Data.CAMPAINGN_ID = requestBody.campaign_id.ToString();
                        edmD.Data.CAMPAINGN_ID = requestBody.campaign_name;
                        edmD.Data.UPDATE_DATE = DateTime.Now;
                        edmD.Edit("0", edmD.Data);
                    }
                }

                mReturn.data = trWebHookHistory.Save(requestBody);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus SendEmailTransactional(SendEmailTransactionalRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            TaxiLogin taxiLogin = new TaxiLogin();
            TaxiSendTransactional taxiSendTransactional = new TaxiSendTransactional();

            try
            {
               var loginData = taxiLogin.Login();
                if (loginData.code == 201 && loginData.data != null)
                {
                    SendTransactionalRequest requestEmail = new SendTransactionalRequest();
                    requestEmail.priority = "0";
                    requestEmail.from_name = request.from_name;
                    requestEmail.from_email = request.from_email;
                    requestEmail.to_name = request.to_name;
                    requestEmail.to_email = request.to_email;
                    requestEmail.subject = request.subject;
                    requestEmail.content_html = request.content_html;
                    requestEmail.content_plain = request.content_plain;
                    requestEmail.transactional_group_name = request.transactional_group_name;
                    requestEmail.message_id = request.message_id;
                    requestEmail.report_type = "Full";
                    requestEmail.session_id = loginData.data.session_id;
                    var taxiSendEmail = taxiSendTransactional.SendTransactional(requestEmail);

                    if (taxiSendEmail.code == 202 && taxiSendEmail.data != null)
                    {
                        mReturn.data = taxiSendEmail;
                    }
                    else
                    {
                        mReturn.success = loginData.status == "error" ? false : true;
                        mReturn.message = loginData.err_msg;
                        mReturn.code = loginData.code;
                    }
                }
                else
                {
                    mReturn.success = loginData.status == "error"  ? false : true;
                    mReturn.message = loginData.err_msg;
                    mReturn.code = loginData.code;
                }
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }


        public ActionResultStatus SendEmailTransactionalTemplate(SendEmailTransactionalTemplateRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            TaxiLogin taxiLogin = new TaxiLogin();
            TaxiSendTransactionalTemplate taxiSendTransactionalMail = new TaxiSendTransactionalTemplate();
            try
            {
                var loginData = taxiLogin.Login();
                //if (loginData.code == 201 && loginData.data != null)
                //{
                    SendTransactionalTemplateRequest requestEmail = new SendTransactionalTemplateRequest();
                    requestEmail.priority = "0";
                    requestEmail.from_name = request.from_name;
                    requestEmail.from_email = request.from_email;
                    requestEmail.to_name = request.to_name;
                    requestEmail.to_email = request.to_email;
                    requestEmail.subject = request.subject;
                    requestEmail.content_html = request.content_html;
                    requestEmail.template_key = request.template_key;
                    requestEmail.transactional_group_name = request.transactional_group_name;
                    requestEmail.message_id = request.message_id;
               
                    requestEmail.report_type = "Full";
                    requestEmail.session_id = "8429de6bd4a5e4f724195d0b6827729f";
                    var taxiSendEmail = taxiSendTransactionalMail.SendTransactionalTemplate(requestEmail);

                    if (taxiSendEmail.code == 202 && taxiSendEmail.data != null)
                    {
                        mReturn.data = taxiSendEmail;
                    }
                    else
                    {
                        mReturn.success = loginData.status == "error" ? false : true;
                        mReturn.message = loginData.err_msg;
                        mReturn.code = loginData.code;
                    }
                //}
                //else
                //{
                //    mReturn.success = loginData.status == "error" ? false : true;
                //    mReturn.message = loginData.err_msg;
                //    mReturn.code = loginData.code;
                //}
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

    }
}