﻿using INCENTIVE_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using DB_MODEL;

namespace INCENTIVE_API.MessageHandlers
{
    public class APIKeyHandler:DelegatingHandler
    {
        //set a default API key 
        private const string yourAPP_TOKEN = "testtoken";
        private const string yourApp_username = "";
        private const string yourApp_userpassword = "";
        public  string APP_NAME = "";

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            bool isValidAPIKey = false;
            IEnumerable<string> lsHeaders;
            //Validate that the api key exists
            string except = "Bad TOKEN.";
            var checkApiKeyExists = request.Headers.TryGetValues("token", out lsHeaders);

            if (checkApiKeyExists)
            {
               var APP_TOKEN= lsHeaders.FirstOrDefault();
                try
                {
                    using (var context = new ModelContext())
                    {
                        var query = context.AppAuths.Where(s => s.APP_TOKEN == APP_TOKEN)
                                           .FirstOrDefault<AppAuth>();
                        if (query != null)
                        {
                            APP_NAME = query.APP_NAME;
                            isValidAPIKey = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log4.log.Info(ex);
                    except = "Is Exception.";
                    isValidAPIKey = false;
                }
            }

            //If the key is not valid, return an http status code.
            if (!isValidAPIKey)
                return request.CreateResponse(HttpStatusCode.Forbidden, except);

            //Allow the request to process further down the pipeline
            var response = await base.SendAsync(request, cancellationToken);

            //Return the response back up the chain
            return response;
        }
    }
}