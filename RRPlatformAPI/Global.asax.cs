﻿using RRApiFramework;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RRPlatFormAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            //if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            //{
            //    //HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache");
            //    //HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
            //    //HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "*");
            //    //HttpContext.Current.Response.AddHeader("Access-Control-Allow-Credentials", "*");
            //    //HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
            //    HttpContext.Current.Response.End();
            //}
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.MessageHandlers.Add(new TokenValidationHandler());

        }
    }
}
//'System.Web.Http.Cors, Version=5.2.6.0