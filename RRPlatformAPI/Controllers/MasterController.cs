﻿using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using System.Web.Http;

namespace RRPlatFormAPI.Controllers
{
    public class MasterController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Master";
            }
        }

        [Route("api/getMasterList")] //
        [HttpPost]
        public IHttpActionResult GetMasterList([FromBody]GetMasterListRequest model)
        {
            this.MethodName = "getMasterList";
            MasterBL masterBL = new MasterBL();
            this.InputRequest(model);
            return this.SendResponse(masterBL.GetMasterList(model));
        }


    }
}
