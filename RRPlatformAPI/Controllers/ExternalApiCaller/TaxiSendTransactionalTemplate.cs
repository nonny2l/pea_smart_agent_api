﻿using Newtonsoft.Json.Linq;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.Models.RepositoryModel;
using System.Web.Configuration;

namespace RRPlatFormAPI.Controllers.ExternalApiCaller
{

  
   
    public class TaxiSendTransactionalTemplate : AbRequestApiCaller<TaxiResponse>
    {
        public override string RequestChannel
        {
            get { return " TaxiSend"; }
        }
        public TaxiResponse SendTransactionalTemplate(SendTransactionalTemplateRequest request)
        {
            this.MethodName = "sendTransactionalTemplate";
            this.ContentType = ApiContentType.ApplicationJson.Value();
            this.Accept = ApiContentType.ApplicationJson.Value();
            this.Method = ApiMethod.POST.Value();
            this.EndpointRefer = WebConfigurationManager.AppSettings["ENDPOINT_SEND_TRANSECTIONAL_TEMPLATE"].ToString();
            this.EnableAuthorization = false;
            this.DataInput = request;

            return this.SendRequest();
        }

    }

}