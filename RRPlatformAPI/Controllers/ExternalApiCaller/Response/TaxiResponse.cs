﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Controllers.ExternalApiCaller
{
    public class TaxiResponse
    {
        public TaxiResponse()
        {
            data = new TaxiData();
        }

        public string status { get; set; }
        public int code { get; set; }
        public string err_msg { get; set; }
        public TaxiData data { get; set; }
    }

    public class TaxiData
    {
        public string message_id { get; set; }
        public string claimed { get; set; }
        public string expire { get; set; }
        public string user_type { get; set; }
        public string session_id { get; set; }
    }
}