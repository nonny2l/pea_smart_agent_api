﻿using Newtonsoft.Json.Linq;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.Models.RepositoryModel;
using System.Web.Configuration;

namespace RRPlatFormAPI.Controllers.ExternalApiCaller
{

  
    public class TaxiSendTransactional : AbRequestApiCaller<TaxiResponse>
    {
        public override string RequestChannel
        {
            get { return " TaxiSend"; }
        }
        public TaxiResponse SendTransactional(SendTransactionalRequest request)
        {
            this.MethodName = "sendTransactional";
            this.ContentType = ApiContentType.ApplicationJson.Value();
            this.Accept = ApiContentType.ApplicationJson.Value();
            this.Method = ApiMethod.POST.Value();
            this.EndpointRefer = WebConfigurationManager.AppSettings["ENDPOINT_SEND_TRANSECTIONAL"].ToString();
            this.EnableAuthorization = false;
            this.DataInput = request;

            return this.SendRequest();
        }

    }

}