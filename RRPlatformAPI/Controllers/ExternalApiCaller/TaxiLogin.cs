﻿using Newtonsoft.Json.Linq;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.Models.RepositoryModel;
using System.Web.Configuration;

namespace RRPlatFormAPI.Controllers.ExternalApiCaller
{

    public class TaxiLogin : AbRequestApiCaller<TaxiResponse>
    {
        public override string RequestChannel
        {
            get { return " TaxiSend"; }
        }

        public TaxiResponse Login()
        {
            this.MethodName = "login";
            this.ContentType = ApiContentType.ApplicationJson.Value();
            this.Accept = ApiContentType.ApplicationJson.Value();
            this.Method = ApiMethod.POST.Value();
            this.EndpointRefer = WebConfigurationManager.AppSettings["ENDPOINT_LOGIN"].ToString();
            string userName = WebConfigurationManager.AppSettings["TAXI_USER_NAME"].ToString();
            string password = WebConfigurationManager.AppSettings["TAXI_USER_PASSWORD"].ToString();
            this.EnableAuthorization = false;

            LoginRequest request = new LoginRequest();
            request.username = userName;
            request.password = password;

            this.DataInput = request;

            return this.SendRequest();
        }
    }
 
}