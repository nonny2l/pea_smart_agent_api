﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Controllers.ExternalApiCaller
{
    public class LoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class SendTransactionalRequest
    {
        public string priority { get; set; }
        public string from_name { get; set; }
        public string from_email { get; set; }
        public string to_name { get; set; }
        public string to_email { get; set; }
        public string subject { get; set; }
        public string content_html { get; set; }
        public string content_plain { get; set; }
        public string transactional_group_name { get; set; }
        public string message_id { get; set; }
        public string report_type { get; set; }
        public string session_id { get; set; }
    }

    public class SendTransactionalTemplateRequest
    {
        public string priority { get; set; }
        public string from_name { get; set; }
        public string from_email { get; set; }
        public string to_name { get; set; }
        public string to_email { get; set; }
        public string subject { get; set; }
        public string template_key { get; set; }
        public contentHtmlDataEdm01 content_html { get; set; }
        public string transactional_group_name { get; set; }
        public string message_id { get; set; }
        public string report_type { get; set; }
        public string session_id { get; set; }
    }
    public class contentHtmlDataEdm01
    {
        public string url { get; set; }
        public string name { get; set; }
        public string qrcode { get; set; }
    }


}