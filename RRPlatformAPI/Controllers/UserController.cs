﻿using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using System.Web.Http;

namespace RRPlatFormAPI.Controllers
{
    public class UserController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "User";
            }
        }

        [Route("api/login")]
        [HttpPost]
        public IHttpActionResult Login([FromBody]LoginRequest model)
        {
            this.EnableResponseApiSpect = true;
            this.MethodName = "login";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.Login(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/logout")]
        [HttpPost]
        public IHttpActionResult LogOut()
        {
            this.MethodName = "logout";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.LogOut(this.UserAccessApi);
            return this.SendResponse(responseData);
        }
    }
}
