﻿using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using System.Web;
using System.Web.Http;

namespace RRPlatFormAPI.Controllers
{
    [Authorize]
    public class MenuController : AbApiController
    {
        public override string ModuleName
        {
            get { return "Menu"; }
        }

        [Route("api/getMenu")] //
        [HttpPost]
        public IHttpActionResult GetListMainMenu()
        {
            this.MethodName = "getMenu";
            var responseData = MenuMudole.GetMenu(this.UserAccessApi);
            this.InputRequest(this.UserAccessApi);
            return this.SendResponse(responseData);
        }

        [Route("api/getPermittionActionList")] //
        [HttpPost]
        public IHttpActionResult GetPermittionActionList([FromBody] PermittionListRequestInput model)
        {
            this.MethodName = "GetPermittionActionList";
            var myPromotionList = MenuMudole.GetPromotionActionlist(model);
            this.InputRequest(model);
            return this.SendResponse(myPromotionList);
        }

        [Route("api/getPermittionBottonPerPage")]
        [HttpPost]
        public IHttpActionResult GetPermittionBottonPerPage([FromBody]PermittionBottonPerPageRequestInput model)
        {
            this.MethodName = "GetPermittionBottonPerPage";
            var myPromotionList = MenuMudole.GetPromotionButtonPerPage(model);
            this.InputRequest(model);
            return this.SendResponse(myPromotionList);
        }

        [Route("api/getMenuAll")]
        [HttpPost]
        public IHttpActionResult GetMenuAll([FromBody]GetMenuAllRequest model)
        {
            this.MethodName = "getMenuAll";
            this.InputRequest(model);
            ActionResultStatus responseData = MenuMudole.GetMenuAll(model);
            return this.SendResponse(responseData);
        }
        [Route("api/setPermissionMenu")]
        [HttpPost]
        public IHttpActionResult SetPermissionMenu([FromBody]SetPermissionMenuRequest model)
        {
            this.MethodName = "setPermissionMenu";
            this.InputRequest(model);
            ActionResultStatus responseData = MenuMudole.SetPermissionMenu(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getMenuButtomAll")]
        [HttpPost]
        public IHttpActionResult getMenuButtomAll([FromBody]GetMenuAllRequest model)
        {
            this.MethodName = "getMenuButtomAll";
            this.InputRequest(model);
            ActionResultStatus responseData = MenuMudole.GetMenuButtomAll(model);
            return this.SendResponse(responseData);
        }
    }
}