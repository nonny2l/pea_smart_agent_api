﻿using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using System.Web;
using System.Web.Http;

namespace RRPlatFormAPI.Controllers
{
    public class TaxiMailController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "TaxiMail";
            }
        }

        [Route("api/getWebHookHistory")]
        [HttpPost]
        public IHttpActionResult GetWebHookHistory()
        {
            this.MethodName = "getWebHookHistory";
            TaxiBL taxiBL = new TaxiBL();
            ActionResultStatus responseData = taxiBL.GetWebHookHistory(HttpContext.Current.Request.Form);
            this.InputRequest(HttpContext.Current.Request.Form["report"]);
            return this.SendResponse(responseData);
        }

        [Route("api/sendEmailTransactional")]
        [HttpPost]
        public IHttpActionResult SendEmailTransactional([FromBody]SendEmailTransactionalRequest model)
        {
            this.MethodName = "sendEmailTransactional";
            TaxiBL taxiBL = new TaxiBL();
            ActionResultStatus responseData = taxiBL.SendEmailTransactional(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }


        [Route("api/sendEmailTransactionalTemplate")]
        [HttpPost]
        public IHttpActionResult SendEmailTransactionalTemplate([FromBody]SendEmailTransactionalTemplateRequest model)
        {
            this.MethodName = "sendEmailTransactionalTemplate";
            TaxiBL taxiBL = new TaxiBL();
            ActionResultStatus responseData = taxiBL.SendEmailTransactionalTemplate(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

    }
}
