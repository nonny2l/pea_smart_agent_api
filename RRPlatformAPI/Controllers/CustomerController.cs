﻿using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using System.Web.Http;
using System.Security.Claims;
using RRApiFramework;
using Newtonsoft.Json;

namespace RRPlatFormAPI.Controllers
{
    public class CustomerController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Customer";
            }
        }

        [Route("api/getMasterCustomerStatus")]
        [HttpGet]
        public IHttpActionResult GetMasterCustomerStatus()
        {
            this.MethodName = "getMasterCustomerStatus";
            CustomerBL customerBL = new CustomerBL();
            ActionResultStatus responseData = customerBL.GetMasterCustomerStatus();
            return this.SendResponse(responseData);
        }

        [Route("api/saveImportCustomer")]
        [HttpPost]
        public IHttpActionResult SaveImportCustomer([FromBody]SaveImportCustomerRequest model)
        {
            this.MethodName = "saveImportCustomer";
            CustomerBL customerBL = new CustomerBL();
            this.InputRequest(model);
            ActionResultStatus responseData = customerBL.SaveImportCustomer(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getCustomerList")]
        [HttpPost]
        public IHttpActionResult GetCustomerList([FromBody]GetCustomerListRequest model)
        {
            this.MethodName = "getCustomerList";
            CustomerBL customerBL = new CustomerBL();
            this.EnableResponseApiSpect = true;
            this.InputRequest(model);
            ActionResultStatus responseData = customerBL.GetCustomerList(model);
            return this.SendResponse(responseData);
        }

        [Route("api/updateCustomerStatus")]
        [HttpPost]
        public IHttpActionResult UpdateCustomerStatus([FromBody]UpdateCustomerStatusRequest model)
        {
            this.MethodName = "updateCustomerStatus";
            CustomerBL customerBL = new CustomerBL();
            this.InputRequest(model);
            ActionResultStatus responseData = customerBL.UpdateCustomerStatus(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveAppointment")]
        [HttpPost]
        public IHttpActionResult SaveAppointment([FromBody]SaveAppointmentRequest model)
        {
            this.MethodName = "saveAppointment";
            CustomerBL customerBL = new CustomerBL();
            this.InputRequest(model);
            ActionResultStatus responseData = customerBL.GetMasterCustomerStatus();
            return this.SendResponse(responseData);
        }

        [Route("api/getAppointmentList")]
        [HttpPost]
        public IHttpActionResult GetAppointmentList([FromBody]GetAppointmentListRequest model)
        {
            this.MethodName = "getAppointmentList";
            CustomerBL customerBL = new CustomerBL();
            this.InputRequest(model);
            ActionResultStatus responseData = customerBL.GetAppointmentList(model);
            return this.SendResponse(responseData);
        }

        

    }
}
