﻿using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
   
    public class SaveImportCustomerRequest
    {
        public List<CustomerProfileDto> customerList { get; set; }
    }

    public class GetCustomerListRequest
    {
        [Description("รหัส campianCode")]
        public string campianCode { get; set; }

        [Description("เลขผู้ใช้ไฟ")]
        public string cano { get; set; }

        [Description("รหัสบริษัทลูกค้า")]
        public string customerNo { get; set; }

        [Description("เบอร์โทร 1")]
        public string tel1 { get; set; }

        [Description("เบอร์โทร 2")]
        public string tel2 { get; set; }

        [Description("เบอร์โทร 3")]
        public string tel3 { get; set; }

        [Description("สถานะลูกค้า")]
        public string customerStatus { get; set; }
    }

    public class UpdateCustomerStatusRequest
    {
        public List<UpdateCustomerStatusData> customerList { get; set; }
    }
    public class UpdateCustomerStatusData
    {
        [Description("รหัสบริษัทลูกค้า")]
        public string customerNo { get; set; }
        [Description("สถานะลูกค้า")]
        public string customerStatus { get; set; }
    }

    public class SaveAppointmentRequest : TrAppointmentDto
    {
    }

    public class GetAppointmentListRequest 
    {
        public string customerId { get; set; }
    }
    


}




