﻿using RRPlatFormAPI.Controllers.ExternalApiCaller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
   

    public class GetWebHookHistoryRequest
    {
        public GetWebHookHistoryData report { get; set; }
    }

    public class GetWebHookHistoryData
    {
        public string message_id { get; set; }
        public string action_type { get; set; }
        public string email { get; set; }
        public string source { get; set; }
        public string device { get; set; }
        public string source_transaction { get; set; }
        public int campaign_id { get; set; }
        public string campaign_name { get; set; }
        public string datetime { get; set; }
    }

    public class SendEmailTransactionalRequest 
    {
        public string priority { get; set; }
        public string from_name { get; set; }
        public string from_email { get; set; }
        public string to_name { get; set; }
        public string to_email { get; set; }
        public string subject { get; set; }
        public string content_html { get; set; }
        public string content_plain { get; set; }
        public string transactional_group_name { get; set; }
        public string message_id { get; set; }
    }
    public class SendEmailTransactionalTemplateRequest
    {
        public string priority { get; set; }
        public string from_name { get; set; }
        public string from_email { get; set; }
        public string to_name { get; set; }
        public string to_email { get; set; }
        public string subject { get; set; }
        public string template_key { get; set; }
        public contentHtmlDataEdm01 content_html { get; set; }
        public string transactional_group_name { get; set; }
        public string message_id { get; set; }
    }

}