﻿using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response
{
    public class GetCustomerListResponse : CustomerProfileDto
    {
        public List<TrForrowDto> followList { get; set; }
        public List<TrAppointmentDto> appointmentList { get; set; }
    }

    public class FollowList 
    {
        public string customerId { get; set; }
        public string customerNo { get; set; }
        public string caNo { get; set; }
        public string estimateArea { get; set; }
        public string layout { get; set; }
        public string planRoofTop { get; set; }
        public string billing { get; set; }
        public string singelLine { get; set; }
        public string loadTOU { get; set; }
        public string statusSurveyID { get; set; }
    }

    public class AppointmentList
    {
        public string customerId { get; set; }
        public string customerNo { get; set; }
        public string appointmentDate { get; set; }
        public string addressSurvey { get; set; }
        public string venderName { get; set; }
        public string venderCode { get; set; }
        public string status { get; set; }
        
    }

}