﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response
{
    public class UserResponse
    {
        public int provinceCode { get; set; }
        public string provinceName { get; set; }
        public string provinceNameEN { get; set; }
    }

    public class GetMtUserListResponse
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string userGroupId { get; set; }
        public string userTypeId { get; set; }
        public string empNo { get; set; }
        public string email { get; set; }
        public string isActive { get; set; }
    }

}